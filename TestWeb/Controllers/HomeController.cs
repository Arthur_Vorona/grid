﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace TestWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [HttpGet]
        public JsonResult Contact()
        {
            var data = new List<object>
            {
                new {Name="A", Age=1, Info="A"},
                new {Name="B", Age=2, Info="B"},
                new {Name="C", Age=3, Info="C"},
                new {Name="D", Age=4, Info="D"},
                new {Name="E", Age=5, Info="F"}
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}