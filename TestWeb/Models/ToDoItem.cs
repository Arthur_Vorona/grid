﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.Models
{
    public class ToDoItem
    {
        public string Name { get; set; }
        public string Age { get; set; }

        public string Info { get; set; }
    }
}