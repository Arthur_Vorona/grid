﻿namespace TestWeb.Controls.Grid
{
    public class GridAction
    {
        public string EventName { get; set; }

        public string FunctionName { get; set; }
    }
}