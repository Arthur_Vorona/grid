﻿using System.Collections.Generic;

namespace TestWeb.Controls.Grid
{
    public class GridColumn
    {
        public string Name { get; set; }
        public string HeaderName { get; set; }

        public bool HasFilter { get; set; }

        public string FilterOperation { get; set; }

        public List<GridAction> Actions { get; set; }

        public GridColumn(string name)
        {
            Actions = new List<GridAction>();
            Name = name;
        }
    }
}