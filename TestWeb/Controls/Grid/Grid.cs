﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace TestWeb.Controls.Grid
{
    public class Grid<TModel>
    {
        private readonly HtmlHelper _helper;
        protected readonly GridModel<TModel> _model;

        #region Constructors

        private Grid(string gridId, HtmlHelper helper)
        {
            _helper = helper;
            _model = new GridModel<TModel> { GridId = gridId };
        }

        public Grid(string gridId, string dataUrl, HtmlHelper helper) : this(gridId, helper)
        {
            _model.DataUrl = dataUrl;
        }

        public Grid(string gridId, IList<TModel> dataItems, HtmlHelper helper) : this(gridId, helper)
        {
            _model.DataItems = dataItems;
        }

        #endregion

        #region AddColumn

        public Grid<TModel> Column(string columnName)
        {
            _model.Columns.Add(new GridColumn(columnName));

            return this;
        }

        public Grid<TModel> Column(string columnName, string headerName)
        {
            _model.Columns.Add(new GridColumn(columnName) { HeaderName = headerName });

            return this;
        }

        public Grid<TModel> Column(string columnName, bool hasFilter)
        {
            _model.Columns.Add(new GridColumn(columnName) { HasFilter = hasFilter });

            return this;
        }

        public Grid<TModel> Column(string columnName, string headerName, bool hasFilter)
        {
            _model.Columns.Add(new GridColumn(columnName) { HeaderName = headerName, HasFilter = hasFilter });

            return this;
        }


        public Grid<TModel> Column<TProperty>(Expression<Func<TModel, TProperty>> property)
        {
            var expression = (MemberExpression)property.Body;

            Column(expression.Member.Name);

            return this;
        }

        public Grid<TModel> Column<TProperty>(Expression<Func<TModel, TProperty>> property, string headerName)
        {
            var expression = (MemberExpression)property.Body;

            Column(expression.Member.Name, headerName);

            return this;
        }

        public Grid<TModel> Column<TProperty>(Expression<Func<TModel, TProperty>> property, bool hasFilter)
        {
            var expression = (MemberExpression)property.Body;

            _model.Columns.Add(new GridColumn(expression.Member.Name) { HasFilter = hasFilter });

            return this;
        }

        public Grid<TModel> Column<TProperty>(Expression<Func<TModel, TProperty>> property, string headerName, bool hasFilter)
        {
            var expression = (MemberExpression)property.Body;

            _model.Columns.Add(new GridColumn(expression.Member.Name) { HeaderName = headerName, HasFilter = hasFilter });

            return this;
        }

        #endregion

        public Grid<TModel> AddPager(int pageSize = 5)
        {
            _model.RenderPager = true;
            _model.PageSize = pageSize;

            return this;
        }

        public HtmlString Build()
        {
            return _helper.Partial("_Grid", _model);
        }
    }
}