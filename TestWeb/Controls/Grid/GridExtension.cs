﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace TestWeb.Controls.Grid
{
    public static class GridExtension
    {
        #region Simple grid

        public static Grid<object> Grid(this HtmlHelper helper, string gridId, string dataUrl)
        {
            return new Grid<object>(gridId, dataUrl, helper);
        }

        public static Grid<object> Grid(this HtmlHelper helper, string gridId, IList<object> dataItems)
        {
            return new Grid<object>(gridId, dataItems, helper);
        }

        #endregion

        #region Generic grid

        public static Grid<TModel> Grid<TModel>(this HtmlHelper helper, string gridId, string dataUrl)
        {
            return new Grid<TModel>(gridId, dataUrl, helper);
        }

        public static Grid<TModel> Grid<TModel>(this HtmlHelper helper, string gridId, IList<TModel> dataItems)
        {
            return new Grid<TModel>(gridId, dataItems, helper);
        }

        #endregion
    }
}