﻿using System;
using System.Collections.Generic;

namespace TestWeb.Controls.Grid
{
    public class GridModel<TData>
    {
        public string GridId;

        public string DataUrl;

        public IList<GridColumn> Columns;

        public IList<TData> DataItems;

        public string TheadClass;

        public bool RenderHeader;

        public bool RenderPager;

        public int PageSize;

        public string PagerId => RenderPager ? $"{GridId}-pager" : string.Empty;

        public GridModel()
        {
            Columns = new List<GridColumn>();
            DataItems = new List<TData>();
            RenderHeader = true;
        }
    }
}